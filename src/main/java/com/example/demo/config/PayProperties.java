package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: HuYi.Zhang
 * @create: 2018-06-07 11:38
 **/
@ConfigurationProperties(prefix ="modernauto.alipay")
public class PayProperties {
    private String natUrl; //支付宝回调地址

    private String app_id; // 公众账号ID

    private String merchant_private_key; // 商户密钥

    private String alipay_public_key; // 支付宝公钥

    private String return_url; // 同步通知

    private String notify_url;// 异步通知

    private String gatewayUrl;// 支付宝网关

    private String sign_type;// 签名方式

    private String Unicode; //字符编码


    public String getNatUrl() {
        return natUrl;
    }

    public void setNatUrl(String natUrl) {
        this.natUrl = natUrl;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getMerchant_private_key() {
        return merchant_private_key;
    }

    public void setMerchant_private_key(String merchant_private_key) {
        this.merchant_private_key = merchant_private_key;
    }

    public String getAlipay_public_key() {
        return alipay_public_key;
    }

    public void setAlipay_public_key(String alipay_public_key) {
        this.alipay_public_key = alipay_public_key;
    }

    public String getReturn_url() {
        return return_url;
    }

    public void setReturn_url(String return_url) {
        this.return_url = return_url;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getUnicode() {
        return Unicode;
    }

    public void setUnicode(String unicode) {
        Unicode = unicode;
    }

    @Override
    public String toString() {
        return "PayProperties{" +
                "natUrl='" + natUrl + '\'' +
                ", app_id='" + app_id + '\'' +
                ", merchant_private_key='" + merchant_private_key + '\'' +
                ", alipay_public_key='" + alipay_public_key + '\'' +
                ", return_url='" + return_url + '\'' +
                ", notify_url='" + notify_url + '\'' +
                ", gatewayUrl='" + gatewayUrl + '\'' +
                ", sign_type='" + sign_type + '\'' +
                ", Unicode='" + Unicode + '\'' +
                '}';
    }
}