package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Chenqi
 * @date 2019/11/20
 */

@ConfigurationProperties(prefix ="modernauto.vxpay")
public class WechatProperties {


    private String APPID; // 公众账号ID

    private String MCH_ID; // 商户密钥

    private String KEY; // 支付宝公钥

    private  String SPBILL_CREATE_IP;

    private  String NOTIFY_URL;

    public String getTRADE_TYPE() {
        return TRADE_TYPE;
    }

    public void setTRADE_TYPE(String TRADE_TYPE) {
        this.TRADE_TYPE = TRADE_TYPE;
    }

    private  String PLACEANORDER_URL;

    private  String TRADE_TYPE;

    public String getAPPID() {
        return APPID;
    }

    public String getSPBILL_CREATE_IP() {
        return SPBILL_CREATE_IP;
    }

    public void setSPBILL_CREATE_IP(String SPBILL_CREATE_IP) {
        this.SPBILL_CREATE_IP = SPBILL_CREATE_IP;
    }

    public String getNOTIFY_URL() {
        return NOTIFY_URL;
    }

    public void setNOTIFY_URL(String NOTIFY_URL) {
        this.NOTIFY_URL = NOTIFY_URL;
    }

    public String getPLACEANORDER_URL() {
        return PLACEANORDER_URL;
    }

    public void setPLACEANORDER_URL(String PLACEANORDER_URL) {
        this.PLACEANORDER_URL = PLACEANORDER_URL;
    }

    public void setAPPID(String APPID) {
        this.APPID = APPID;
    }

    public String getMCH_ID() {
        return MCH_ID;
    }

    public void setMCH_ID(String MCH_ID) {
        this.MCH_ID = MCH_ID;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }
}
