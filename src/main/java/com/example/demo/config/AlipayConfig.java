/*
package com.example.demo.config;

*/
/**
 * @Classname AlipayController
 * @Description notify_url 和 return_url 需要外网可以访问，建议natapp 内网穿透
 * @Date 2019/3/21 20:40
 * @Created by 爆裂无球
 *//*

public class AlipayConfig {
    //这里用natapp内外网穿透
    public static final String natUrl = "http://localhost:8060";

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016101400687898";//在后台获取（必须配置）

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFIi+TeOLRV61lWjE2M5sJcq3SLpMmk4dNrYbaVIe+w9roDD+bkNoaNSSmFqARlA2OM5D6WdCGzFu7GU3+g3APikxeIKsn/eWI3ku/6dAnX5gy0NcTxZnLI/JmJPCrz3fNjZFKBqeh68FxgH+2WAm9jxthX5wR6baRKUEYQb+gVcgDvI+CqNDUzjjBaPSOKSDMIITej02tJOGAVt2+odhDfWlLybu6wW9A5GHCImJI90EDOx0wETVUXC3EJcZ1ayHOCUVKjgEUAK4hhTSNnsvimv3LMJdrT1HzUPaXY7DMtcdvMVHiEE6WrgGPgR3vS8Aey39SdceuToitVsXkiaCXAgMBAAECggEABfuIScc34hJfj1tjNL1S7TTDDNvc7vHYNaxjj/IWe4MPljfer93tfxn+QJxpqwCNL2Ov0yAwF66dPKYhdBtx8LDVR7GOH2jwZJsgqkoWeX55N2ddEt1hUNiXrvFgA0h6aLlA2yO7arJOagxNMON55JPGtjR2rfNjqdhBvZZOxySjBr9XVx2XAisiB99HMh69b2cbmlmlZ9gX4auUVgrK/tB2NadmWUXIupzUZOACdW0LIZ4bMBC1cHLojbDfYlBvzyVn9FyOjPej10H7C65Mtcv1OzEfNJ7yO41PMMtrzW2fpjY9EKhGfxPimLsvppaEBbtT+Ld9kI8fldR/Cr9hcQKBgQDIzLqsyIRe2zDEudJ+WaRAIIFyMVN+vJfzvzC/fPw0ljN4Mlk44aQbuV58+F/V2L3hWBGZ8PX19AEDJzKZVmnThTTCqtWC4z70urxEuqGL+UILG/44eOqK7kRcHDOzOYFXkL7apve4DLdtRQ40kAX7q53AOe3I4wVKW7KC+l52XwKBgQCpu3PAnK4gdWMFXwJrTf6G8wc2BCOTsyL9Ev56mxVsmW4DsRL6OjNRlR87euHQNB0pMeDbz2oL5uOOAWaFmLftvq81trY9juHRJDbfguZEVh853VMFWPROft+vP2pag1jaSCj9BUI2dDLC1IIA8vJwMja5yhU6KTHCtnhJlj9QyQKBgANL+TSlBidYifBw7JfOePfkkLuDO8+04PnLhc1fC/UBEk9C5FweuEcMQBTlLw2fxV5Rx9gzMJadLvcSSa5i0NH8OeWIUIEZogr6leS6+7QEn6T3DLF4qsElMFIi1GZ45zq80aVdYGvRKaHNpHIQe0iqhKiHSvQlZgzWfJPR8IMZAoGAP+ENsmsFWa/0QXw9c9cZuP5UM28hWKAtxeNVJR2i1Yl1DyyggM5QrB6cb41S2UITy4I7J18LgUk3USWTySnt5ytB08xdEBPrRwQDJuGj+goTnHqAmV2eLMCxPMp4I3KViSlpS7ij78yRoiPKfcLxnWo4kHnOvWHtmWOFJ+TFFYkCgYB7v5RD0goR9EzhpqzQttA59mh7KZzYI3n1YKDYEmI7WqRgAH0US7KnKlpII0WQ/obCT7/9jl0qrCN2ajGl6x+qlM0CW6wSB7bjUiK8z7jg9TSqusdd8oT82YTTEwtI8w9Z/sMPVWmMdeFZ8p3OHsS7g8+7q5nzYDj+G8oaVeugOg==";//教程查看获取方式（必须配置）

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.html 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwIHKa0wY9l+UCApbKqY37SZ9wwErTIWLJPe63vjSxvdZQWPkYP346iW2EeJ1IKeuQifkBaOYCneeXuBNH/CMG6IJkBfccmzO14e+uVj90uJDw5PinckgXbwhJhgaKUXN1Q8KZOkvKrkzsKc3yKHhoLi1z74dFG/asRORvUuCHyI0S4m6A2OuMlTx83cKNm9pSYqVslmOw+hnhQTyuZnSsQ+1Lvw6QH7VDayBDSKJwHS0PAHIirzXfRoMayk1FnngdrsmFaRm7yljlhOtJkbqnsZb7I0XL/XJyGw3YcfG1Te+bIlRo3bCbVggtRJJqAy9EHQp4IrfGWmbW4FDPXbvDQIDAQAB" ;

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = natUrl + "/alipay/alipayNotifyNotice";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = natUrl + "/alipay/alipayReturnNotice";
//    public static String return_url = "http://login.calidray.com/?#/sign";
    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";//注意：沙箱测试环境，正式环境为：https://openapi.alipay.com/gateway.do
}
*/
