package com.example.demo.dao;

import com.example.demo.pojo.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

/**
 * @Classname IOrderDao
 * @Description TODO
 * @Date 2019/3/21 20:50
 * @Created by 爆裂无球
 */
public interface IOrderDao {


    @Insert("insert into orders (id, order_num, order_status, \n" +
            "      order_amount, paid_amount, product_id, \n" +
            "      buy_counts, create_time, paid_time\n" +
            "      )\n" +
            "    values (#{id,jdbcType=VARCHAR}, #{orderNum,jdbcType=VARCHAR}, #{orderStatus,jdbcType=VARCHAR}, \n" +
            "      #{orderAmount,jdbcType=VARCHAR}, #{paidAmount,jdbcType=VARCHAR}, #{productId,jdbcType=VARCHAR}, \n" +
            "      #{buyCounts,jdbcType=INTEGER}, #{createTime,jdbcType=TIMESTAMP}, #{paidTime,jdbcType=TIMESTAMP}\n" +
            "      )")
    void insert(Orders order);


    Orders getOrderById(@Param("orderId") String orderId);


    int updateById(Orders orders);
}
