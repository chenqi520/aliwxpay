package com.example.demo.dao;

import com.example.demo.pojo.Product;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Classname IProductdao
 * @Description TODO
 * @Date 2019/3/21 20:50
 * @Created by 爆裂无球
 */
public interface IProductdao {


    @Select("select * from product ")
    List<Product> getAllProduct();

    @Select("select * from product where id=#{id}")
    Product getProductById(String id);


}
