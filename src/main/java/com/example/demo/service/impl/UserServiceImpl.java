package com.example.demo.service.impl;

import com.example.demo.pojo.User;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Classname UserServiceImpl
 * @Description TODO
 * @Date 2019/3/21 20:46
 * @Created by 爆裂无球
 */
@Service
public class UserServiceImpl implements UserService {



    @Override
    public void saveUser(User user) {

    }

    @Override
    public void updateUserById(User user) {

    }

    @Override
    public void deleteUserById(String userId) {

    }

    @Override
    public User getUserById(String userId) {
        return null;
    }

    @Override
    public List<User> getUserList() {
        return null;
    }
}
