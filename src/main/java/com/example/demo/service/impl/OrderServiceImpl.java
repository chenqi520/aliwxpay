package com.example.demo.service.impl;

import com.example.demo.dao.IFlowDao;
import com.example.demo.dao.IOrderDao;
import com.example.demo.enums.OrderStatusEnum;
import com.example.demo.pojo.Flow;
import com.example.demo.pojo.Orders;
import com.example.demo.service.OrdersService;
import idworker.Sid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Classname OrderServiceImpl
 * @Description TODO
 * @Date 2019/3/21 20:47
 * @Created by 爆裂无球
 */
@Service
public class OrderServiceImpl implements OrdersService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private IOrderDao iOrderDao;

    @Autowired
    private IFlowDao iFlowDao;


    @Override
    public void saveOrder(Orders order) {
        iOrderDao.insert(order);
    }

    @Override
    @Transactional
    public void updateOrderStatus(String orderId, String alpayFlowNum, String paidAmount) {
        Orders order = getOrderById(orderId);
        //如果是 待付款 修改订单状态 并且 并且 新增  流水
        if (order.getOrderStatus().equals(OrderStatusEnum.WAIT_PAY.key)) {
            order = new Orders();
            order.setId(orderId);
            order.setOrderStatus(OrderStatusEnum.PAID.key);
            order.setPaidTime(new Date());
            order.setPaidAmount(paidAmount);

            iOrderDao.updateById(order);

            //查询    修改后最新的order
            order = getOrderById(orderId);
            Sid sid = new Sid();
            String flowId = sid.nextShort();
            Flow flow = new Flow();
            flow.setId(flowId);
            flow.setFlowNum(alpayFlowNum);
            flow.setBuyCounts(order.getBuyCounts());
            flow.setCreateTime(new Date());
            flow.setOrderNum(orderId);
            flow.setPaidAmount(paidAmount);
            flow.setPaidMethod(1);
            flow.setProductId(order.getProductId());
            //插入 流水表
            iFlowDao.insert(flow);

        }

    }

    @Override
    public Orders getOrderById(String orderId) {
        LOGGER.info("开始查询订单，订单orderId:{}", orderId);
        return iOrderDao.getOrderById(orderId);
    }
}
