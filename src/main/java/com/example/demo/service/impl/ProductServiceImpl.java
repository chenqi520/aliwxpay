package com.example.demo.service.impl;

import com.example.demo.dao.IProductdao;
import com.example.demo.pojo.Product;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Classname ProductServiceImpl
 * @Description TODO
 * @Date 2019/3/21 20:48
 * @Created by 爆裂无球
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private IProductdao iProductdao;


    @Override
    public List<Product> getProducts() {
        return iProductdao.getAllProduct();
    }

    @Override
    public Product getProductById(String productId) {
        return iProductdao.getProductById(productId);
    }
}
